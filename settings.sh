
# variables d'environnement
#=============================================


if [ -z "$MAIN" ]
then
    export MAIN="$(realpath $(dirname $0))"
fi

if [ -z "$SRC" ]
then
    export SRC="$(realpath $MAIN/..)"
fi

if [ -z "$WORKSPACE" ]
then
    export WORKSPACE="$(realpath $MAIN/../..)"
fi



# bibliothèques bash
#=============================================



for app in $(find $SRC -type d -name "__bash")
do
    for library in $(find $app -type f -name "*.sh")
    do
        echo $library
        source $library
    done
done


# paramètres des applications
#=============================================


export SEED_IMPORT="$MAIN/settings.sh"

for settings in $(find $SRC -type f -name "__settings__.sh")
do
    export app=$(dirname $settings)
    #echo $settings
    source $settings
done



# executables
#=============================================



for app in $(find $SRC -type d -name "__bin")
do
    #echo $app
    export PATH=$app:$PATH
done
